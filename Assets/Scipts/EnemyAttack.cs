﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour {

    float damage = 0;
    private void Awake()
    {
        damage = GetComponentInParent<EnemyAI_BASE>().Properties.damage;
    }
    //Oyuncu düşmana değerse, bu kod oyuncunun Game Object'ini disable eder.
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Hit");
            PlayerController.Instance.GotHit(damage);
        }
    }
}
