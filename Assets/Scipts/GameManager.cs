﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }
    private GameObject player;
    [SerializeField]
    PlayerProperties playerProperties;

    public PlayerProperties PlayerProperties
    {
        get
        {
            return playerProperties;
        }

        set
        {
            playerProperties = value;
        }
    }

    public GameObject Player
    {
        get
        {
            return player;
        }
    }

    // Use this for initialization
    void Awake ()
    {
        if (instance == null)
            instance = this;
        else
            DestroyImmediate(gameObject);

        player = GameObject.FindGameObjectWithTag("Player");
	
	}
	
	// Update is called once per frame
	void OnGUI () {

        if (player.activeSelf == false)
        {
            if (GUI.Button(new Rect(50, 50, 150, 100), "Restart"))
                SceneManager.LoadScene("TestScene");
        }
    }
}

[System.Serializable]
public class PlayerProperties
{
    public int totalHitPoints;
    public float waitBetweenComboHits;
    public int maxComboState;
    public float damage;
}
