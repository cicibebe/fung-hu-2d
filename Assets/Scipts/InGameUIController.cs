﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUIController : MonoBehaviour {


    public Slider playerHPSlider;

    private void OnEnable()
    {
        EventManager.OnPlayerGotHit += EventManager_OnPlayerGotHit;
    }
    private void OnDisable()
    {
        EventManager.OnPlayerGotHit -= EventManager_OnPlayerGotHit;
    }

    private void EventManager_OnPlayerGotHit(float remainedHP)
    {
        playerHPSlider.value = remainedHP / GameManager.Instance.PlayerProperties.totalHitPoints;
    }
}
