﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

    public GameObject player;
    public Camera main_camera;
    public float smooth = 5f;
    public float shakeDurationOriginal = 0.5f;


    private void OnEnable()
    {
        EventManager.OnCameraShake += EventManager_OnCameraShake;
    }

    private void OnDisable()
    {
        EventManager.OnCameraShake -= EventManager_OnCameraShake;
    }

    private void EventManager_OnCameraShake(float duration)
    {
        CameraShakeCommand(duration);
    }

    void Update()
    {
        if (player != null)
        {
            
            float player_x = player.transform.position.x;
            float player_y = player.transform.position.y;

            float rounded_x = RoundToNearestPixel(player_x);
            float rounded_y = RoundToNearestPixel(player_y);

            Vector3 new_pos = new Vector3(rounded_x, rounded_y, -10.0f); // this is 2d, so my camera is that far from the screen.
            main_camera.transform.position = Vector3.Lerp(transform.position, new_pos, smooth * Time.deltaTime);
        }
    }
    public float pixelToUnits = 40f;

    public float RoundToNearestPixel(float unityUnits)
    {
        float valueInPixels = unityUnits * pixelToUnits;
        valueInPixels = Mathf.Round(valueInPixels);
        float roundedUnityUnits = valueInPixels * (1 / pixelToUnits);
        return roundedUnityUnits;
    }

    public void CameraShakeCommand(float _duration)
    {
        float duration = _duration != 0 ? _duration : shakeDurationOriginal;
        StartCoroutine(CameraShake(duration));
    }

    public IEnumerator CameraShake(float shakeDuration)
    {

        while (shakeDuration > 0.1f)
        {
            Vector2 rotationAmount = Random.insideUnitCircle * 0.5f;
            main_camera.transform.localRotation = Quaternion.Euler(rotationAmount);
            shakeDuration -= 0.1f;
            yield return null;
        }
        transform.localRotation = Quaternion.identity;
        yield return null;
    }
}
