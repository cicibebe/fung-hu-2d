﻿using System.Collections;

public interface IEnemyStateMachine
{

    IEnumerator EnemyStateMachine();
    
    IEnumerator Patrol();
    IEnumerator ReturnToPatrol();
    IEnumerator Search();
    IEnumerator Wait();
    IEnumerator Chase();
    IEnumerator Attack();
    IEnumerator Hitstun();
    IEnumerator Stun();
    IEnumerator Recover();
}
