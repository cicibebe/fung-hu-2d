﻿using UnityEngine;
using System.Collections;

public class PlayerAttackCollider : MonoBehaviour
{
    private PlayerAttack playerAttack;

    void Start()
    {
        playerAttack = GetComponentInParent<PlayerAttack>();
    }

    void OnEnable()
    {
        StartCoroutine(ActivateColliderForOneFrame());
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy" && playerAttack.comboState < 1)
        {
            playerAttack.enemyList.Add(other.gameObject);
            playerAttack.comboState++;
        }
    }

    IEnumerator ActivateColliderForOneFrame()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        Disable();
    }

    void Disable()
    {
        gameObject.SetActive(false);
    }
}