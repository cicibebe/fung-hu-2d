﻿using UnityEngine;
using System.Collections;

public class EnemyAI : EnemyAI_BASE
{
    public int hitPoints = 9;

    public float nextCycle;
    public float nextCycleRate = 4f;
    private float previousSpeed = -1f;


    public Transform enemyEyes;
    public GameObject enemyVision;
    public GameObject attackCollider;

    //State Machine ile ilgili.
    public float waitStateDuration = 2f;
    private float hitDistance = 0.5f;
    private float waitTimer;
    private Vector2 playerLastSeen;
    public bool playerSeen = false;

    void Start()
    {
        Init();
    }

    //State Machine WIP
    public override IEnumerator EnemyStateMachine()
    {
        while (this)
        {
            switch (currentState)
            {
                case EnemyState.Patrolling:
                    yield return Patrol();
                    break;
                case EnemyState.ReturningToPatrol:
                    yield return ReturnToPatrol();
                    break;
                case EnemyState.Searching:
                    yield return Search();
                    break;
                case EnemyState.Waiting:
                    yield return Wait();
                    break;
                case EnemyState.Chasing:
                    yield return Chase();
                    break;
                case EnemyState.Attacking:
                    yield return Attack();
                    break;
                case EnemyState.Recovering:
                    yield return Recover();
                    break;
                case EnemyState.Hitstunned:
                    yield return Hitstun();
                    break;
                case EnemyState.Stunned:
                    yield return Stun();
                    break;
                default:
                    break;
            }
        }
    }

    void RaycastToPlayer()
    {
        //Eğer oyuncu düşmanın görüş mesafesindeyse, bu kod oyuncunun görülüp görülmediğine karar vermek için Raycast kullanır. 
        //Raycast oyuncuya gidene kadar bir engelle karşılaşmıyorsa oyuncu görülmüş sayılır.
        if (vision.playerInRange)
        {
            Debug.Log("RaycastToPlayer");
            RaycastHit2D hit = Physics2D.Linecast(enemyEyes.position, player.transform.position);
            Debug.DrawLine(enemyEyes.position, hit.point, Color.red, 20);
            if (hit.collider != null)
            {
                if (hit.collider.tag == "Player")
                {
                    Debug.Log("RaycastToPlayer:hit.collider.tag");
                    playerSeen = true;
                    playerLastSeen = player.transform.position;
                    currentState = EnemyState.Chasing;
                }
                else
                    playerSeen = false;
            }
        }
        else
            playerSeen = false;
    }

    public override IEnumerator Patrol()
    {
        RaycastToPlayer();
        if (Time.time > nextCycle)
        {
            Flip();
            nextCycle = Time.time + nextCycleRate;
        }
        yield return null;
    }

    public override IEnumerator ReturnToPatrol()
    {
        InvokeRepeating("RaycastToPlayer", 0.1f, 0.5f);
        if (transform.position.x < originPosition.x)
        {
            transform.localScale = localScaleTo_faceRight;
            enemyrb2d.velocity = new Vector2(defaultSpeed, enemyrb2d.velocity.y);
            yield return new WaitUntil(() => playerSeen || Vector2.Distance(transform.position, originPosition) < 0.1f);
        }
        else if (transform.position.x > originPosition.x)
        {
            transform.localScale = localScaleTo_faceLeft;
            enemyrb2d.velocity = new Vector2(-defaultSpeed, enemyrb2d.velocity.y);
            yield return new WaitUntil(() => playerSeen || Vector2.Distance(transform.position, originPosition) < 0.1f);
        }

        CancelInvoke();
        if (playerSeen)
        {
            currentState = EnemyState.Chasing;
            yield return null;
        }
        else if (!playerSeen)
        {
            currentState = EnemyState.Patrolling;
            enemyrb2d.velocity = new Vector2(defaultSpeed, enemyrb2d.velocity.y);
            transform.localScale = localScaleTo_faceRight;
            nextCycle = Time.time + nextCycleRate;
            previousSpeed = 1f;
            yield return null;
        }
    }

    public override IEnumerator Search()
    {
        InvokeRepeating("RaycastToPlayer", 0.1f, 0.5f);
        if (playerLastSeen.x < transform.position.x + 1)
        {
            transform.localScale = localScaleTo_faceLeft;
            enemyrb2d.velocity = new Vector2(-defaultSpeed, enemyrb2d.velocity.y);
            yield return new WaitUntil(() => playerSeen || playerLastSeen.x > transform.position.x - 1);
        }
        else if (playerLastSeen.x > transform.position.x - 1)
        {
            transform.localScale = localScaleTo_faceRight;
            enemyrb2d.velocity = new Vector2(defaultSpeed, enemyrb2d.velocity.y);
            yield return new WaitUntil(() => playerSeen || playerLastSeen.x < transform.position.x - 1);
        }
        CancelInvoke();
        if (playerSeen)
        {
            currentState = EnemyState.Chasing;
            yield return null;
        }
        else if (!playerSeen)
        {
            enemyrb2d.velocity = new Vector2(0, enemyrb2d.velocity.y);
            currentState = EnemyState.Waiting;
            yield return null;
        }
    }

    public override IEnumerator Wait()
    {
        InvokeRepeating("RaycastToPlayer", 0.1f, 0.5f);
        anim.Play("BigMook_Idle_anim");
        while (currentState == EnemyState.Waiting)
        {
            waitTimer += Time.deltaTime;
            if (playerSeen)
            {
                currentState = EnemyState.Chasing;
                yield return null;
            }
            else if (waitTimer > waitStateDuration)
            {
                currentState = EnemyState.ReturningToPatrol;
                yield return null;
            }
            yield return null;
        }
        CancelInvoke();
        waitTimer = 0;
        anim.Play("BigMook_Walk_anim");
        yield return null;
    }

    public override IEnumerator Chase()
    {
        Debug.Log("Chase");
        InvokeRepeating("RaycastToPlayer", 0.1f, 0.5f);
        while (currentState == EnemyState.Chasing)
        {
            Debug.Log("Chase:While");
            if (player.transform.position.x < transform.position.x)
            {
                Debug.Log("Chase:faceLeft");
                transform.localScale = localScaleTo_faceLeft;
                enemyrb2d.velocity = new Vector2(-defaultSpeed, enemyrb2d.velocity.y);
                yield return new WaitUntil(() => !playerSeen || player.transform.position.x > transform.position.x - hitDistance);
            }
            else if (player.transform.position.x > transform.position.x)
            {
                transform.localScale = localScaleTo_faceRight;
                enemyrb2d.velocity = new Vector2(defaultSpeed, enemyrb2d.velocity.y);
                yield return new WaitUntil(() => !playerSeen || player.transform.position.x < transform.position.x + hitDistance);
            }
            CancelInvoke();
            if (Vector2.Distance(player.transform.position, transform.position) <= hitDistance)
            {
                enemyrb2d.velocity = new Vector2(0, enemyrb2d.velocity.y);
                currentState = EnemyState.Attacking;
                yield return null;
            }
            else if (!playerSeen)
            {
                currentState = EnemyState.Searching;
                yield return null;
            }
        }
        yield return null;
    }

    public override IEnumerator Attack()
    {
        //anim.SetTrigger("enemyAttack");
        anim.Play("BigMook_Punch_anim");
        yield return new WaitForSeconds(1f);
        attackCollider.SetActive(true);
        //Charge to player
        yield return new WaitForSeconds(0.25f);
        attackCollider.SetActive(false);
        //Stop
        yield return currentState = EnemyState.Recovering;
    }

    public override IEnumerator Recover()
    {
        anim.Play("BigMook_Stunned_anim");
        yield return new WaitForSeconds(2f);
        yield return currentState = EnemyState.Chasing;
    }

    public override IEnumerator KnockBack()
    {
        anim.Play("BigMook_Stunned_anim");
        yield return new WaitForSeconds(2f);
        yield return currentState = EnemyState.Chasing;
    }

    public override IEnumerator Hitstun()
    {
        hitState++;
        enemyrb2d.drag = 8f;
        if (transform.position.x < player.transform.position.x)
        {
            enemyrb2d.velocity = new Vector2(-4f, 2f);
            transform.localScale = localScaleTo_faceRight;
        }
        else if (transform.position.x > player.transform.position.x)
        {
            enemyrb2d.velocity = new Vector2(4f, 2f);
            transform.localScale = localScaleTo_faceLeft;
        }
        /*if (transform.position.x < player.transform.position.x)
        {
            enemyrb2d.velocity = new Vector2(-knockbackForce * forceMultiplier, knockupForce);
            transform.localScale = faceRight;
        }
        else if (transform.position.x > player.transform.position.x)
        {
            enemyrb2d.velocity = new Vector2(knockbackForce * forceMultiplier, knockupForce);
            transform.localScale = faceLeft;
        }*/
        yield return new WaitForSeconds(0.2f);
        enemyrb2d.drag = 0f;
        yield return new WaitForSeconds(1f);
        hitState = 0;
        currentState = EnemyState.Chasing;
        yield return null;
    }

    public override IEnumerator Stun()
    {
        hitState = 0;
        enemyrb2d.drag = 5f;
        if (transform.position.x < player.transform.position.x)
        {
            enemyrb2d.velocity = new Vector2(-4f, 2f) * 2f;
            transform.localScale = localScaleTo_faceRight;
        }
        else if (transform.position.x > player.transform.position.x)
        {
            enemyrb2d.velocity = new Vector2(4f, 2f) * 2f;
            transform.localScale = localScaleTo_faceLeft;
        }
        yield return new WaitForSeconds(0.8f);
        enemyrb2d.drag = 0f;
        enemyrb2d.velocity = new Vector2(0f, enemyrb2d.velocity.y);
        yield return new WaitForSeconds(1f);
        currentState = EnemyState.Searching;
        yield return null;
    }



    // Update is called once per frame
    /*void FixedUpdate () {

        // Alternatif State Machine, wip
        switch (currentState)
        {
            case EnemyState.Patrolling:          
                break;
            case EnemyState.ReturningToPatrol:
                break;
            case EnemyState.Chasing:
                break;
            case EnemyState.Searching:
                break;
            case EnemyState.Attacking:
                break;
            case EnemyState.Stunned:
                break;
            default:
                break;
        }

        if (!knockBack)
        {
            //Eğer oyuncu düşmanın görüş mesafesindeyse, bu kod oyuncunun görülüp görülmediğine karar vermek için Raycast kullanır. 
            //Raycast oyuncuya gidene kadar bir engelle karşılaşmıyorsa oyuncu görülmüş sayılır.
            if (enemyVision.GetComponent<EnemyVision>().playerInRange)
            {
                RaycastHit2D hit = Physics2D.Linecast(enemyEyes.position, player.transform.position);
                Debug.DrawLine(enemyEyes.position, hit.point, Color.red, 20);
                if (hit.collider.tag == "Player")
                {
                    patrolling = false;
                    playerSeen = true;
                }
                else
                    playerSeen = false;
            }
            else
                playerSeen = false;

            //Eğer Raycast oyuncuya değiyorsa, bu kod düşmanı oyuncunun pozisyonuna doğru götürür.
            if (playerSeen)
            {
                if (player.transform.position.x < transform.position.x)
                {
                    transform.localScale = faceLeft;
                    if (player.transform.position.x <= transform.position.x - 0.3)
                        enemyrb2d.velocity = new Vector2(-defaultSpeed, enemyrb2d.velocity.y);
                    else
                        enemyrb2d.velocity = new Vector2(0, enemyrb2d.velocity.y);
                }
                else if (player.transform.position.x > transform.position.x)
                {
                    transform.localScale = faceRight;
                    if (player.transform.position.x >= transform.position.x + 0.3)
                        enemyrb2d.velocity = new Vector2(defaultSpeed, enemyrb2d.velocity.y);
                    else
                        enemyrb2d.velocity = new Vector2(0, enemyrb2d.velocity.y);
                }
            }

            //Eğer oyuncu görüş mesafesinde değilse veya Raycast oyuncuya değmiyorsa, bu kod düşmanın patrol sekansını çalıştırır.
            if (!playerSeen)
            {
                if (!patrolling)
                {
                    Debug.Log("Returning to patrol.");
                    if (transform.position.x < originPosition.x)
                    {
                        transform.localScale = faceRight;
                        enemyrb2d.velocity = new Vector2(defaultSpeed, enemyrb2d.velocity.y);
                    }
                    else if (transform.position.x > originPosition.x)
                    {
                        transform.localScale = faceLeft;
                        enemyrb2d.velocity = new Vector2(-defaultSpeed, enemyrb2d.velocity.y);
                    }

                    if (Vector2.Distance(transform.position, originPosition) < 0.1f)
                    {
                        Debug.Log("PATROL.");
                        patrolling = true;
                        firstPatrol = true;
                    }
                }
                else if (patrolling)
                {
                    if (firstPatrol)
                    {
                        Debug.Log("Patrolling.");
                        enemyrb2d.velocity = new Vector2(defaultSpeed, enemyrb2d.velocity.y);
                        transform.localScale = faceRight;
                        nextCycle = Time.time + nextCycleRate;
                        firstPatrol = false;
                        flip = false;
                    }

                    else if (Time.time > nextCycle && (!flip))
                    {
                        Debug.Log("bug");
                        enemyrb2d.velocity = new Vector2(-defaultSpeed, enemyrb2d.velocity.y);
                        Flip();
                        nextCycle = Time.time + nextCycleRate;
                        flip = true;
                    }

                    else if (Time.time > nextCycle && (flip))
                    {
                        Debug.Log("bug");
                        enemyrb2d.velocity = new Vector2(defaultSpeed, enemyrb2d.velocity.y);
                        Flip();
                        nextCycle = Time.time + nextCycleRate;
                        flip = false;
                    }
                }
            }
        }
    }*/


}
