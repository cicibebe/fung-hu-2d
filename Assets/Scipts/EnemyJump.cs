﻿using UnityEngine;
using System.Collections;

public class EnemyJump : MonoBehaviour {

    private Rigidbody2D parent;

    public float jumpForce = 4f;
    private bool isJumping = false;

    // Use this for initialization
	void Start () {
        parent = transform.parent.GetComponentInParent<Rigidbody2D>();
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Ground" && !isJumping)
        {
            Debug.Log("Jump");
            isJumping = true;
            StartCoroutine(Jump());
        }
        else
            return;
    }

    public IEnumerator Jump()
    {
        parent.velocity = new Vector2(parent.velocity.x + 0.5f, jumpForce);
        yield return new WaitForSeconds(1f);
        isJumping = false;
        yield return null;
    }
}
