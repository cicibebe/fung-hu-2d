﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

    public delegate void CameraShakeAction(float duration);
    public static event CameraShakeAction OnCameraShake;
    public static void ShakeCamera(float duration)
    {
        if (OnCameraShake != null)
            OnCameraShake(duration);
    }

    public delegate void PlayerGotHitAction(float remainedHP);
    public static event PlayerGotHitAction OnPlayerGotHit;
    public static void PlayerGotHit(float remainedHP)
    {
        if (OnPlayerGotHit != null)
            OnPlayerGotHit(remainedHP);
    }

    public delegate void PlayerDiedAction();
    public static event PlayerDiedAction OnPlayerDied;
    public static void PlayerDied()
    {
        if (OnPlayerDied != null)
            OnPlayerDied();
    }
}
