﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerAttack : MonoBehaviour
{

    private PlayerController controller;

    public GameObject attackCollider;
    public Animator anim;

    public bool isAttacking = false;
    public int comboState = 0;
    private bool canAdvanceCombo = false;
    private int maxComboStateCount;

    public float knockbackForce = 4f;
    public float knockupForce = 2f;

    public List<GameObject> enemyList;
    private float closestEnemyPosition = 5f;
    private GameObject targetEnemy;

    void Awake()
    {
        attackCollider.SetActive(false);
        anim = GetComponent<Animator>();
        controller = GetComponent<PlayerController>();
        enemyList = new List<GameObject>();
        maxComboStateCount = GameManager.Instance.PlayerProperties.maxComboState;
    }

    void Update()
    {

        if (controller.isGrounded)
        {
            if (CrossPlatformInputManager.GetButtonDown("Attack") && comboState < 1 && !isAttacking)
            {
                isAttacking = true;
                Invoke("FreeAttackCD", 1f);
                StartCoroutine(Combo());
            }
            else if (CrossPlatformInputManager.GetButtonDown("Attack") && comboState > 0)
            {
                canAdvanceCombo = true;
            }
        }
        else if (!controller.isGrounded)
        {
            if (CrossPlatformInputManager.GetButtonDown("Attack") && !isAttacking)
            {
                isAttacking = true;
                Invoke("FreeAttackCD", 1f);
                StartCoroutine(Combo());
            }
        }
    }

    private void FreeAttackCD()
    {
            isAttacking = false;
    }

    //Oyuncuyu düşmana doğru kaydırır.
    private void AttackSlide()
    {
        EventManager.ShakeCamera(0.5f);

        foreach (GameObject enemy in enemyList)
        {
            float distance;
            distance = Vector2.Distance(transform.position, enemy.transform.position);
            if (distance < closestEnemyPosition)
            {
                distance = closestEnemyPosition;
                targetEnemy = enemy;
            }
        }
        if (controller.facingRight)
            //transform.position = Vector3.Lerp(transform.position, new Vector3(targetEnemy.transform.position.x - 0.1f, transform.position.y, transform.position.z), slideForce * Time.deltaTime);
            transform.position = new Vector3(targetEnemy.transform.position.x - 0.25f, transform.position.y, transform.position.z);
        else if (!controller.facingRight)
            transform.position = new Vector3(targetEnemy.transform.position.x + 0.25f, transform.position.y, transform.position.z);
        enemyList.Clear();
    }

    /*Bu coroutine parametre olarak comboState integer'ını alır ve attack butonu spam edildikçe 0.4 sn aralıklarla loop eder - ta ki comboState 3 olana veya attack butonuna basılmayana kadar.
    Bunlar olduğu zaman, comboState 0'lanır. Bu sayede coroutine tekrardan loop etmez ve yukarıdaki ComboStateMachine de aynı şekilde sona erer. */
    private IEnumerator Combo()
    {
        attackCollider.SetActive(true);
        anim.SetTrigger("playerStrikea");
        yield return new WaitUntil(() => comboState > 0 || !isAttacking);
        while (comboState > 0)
        {
            //Oyuncuyu düşmana doğru kaydıran methodu çağırır.
            AttackSlide();
            yield return new WaitForSeconds(0.4f);
            if (comboState >= maxComboStateCount)
            {
                yield return new WaitForSeconds(0.4f);
                comboState = 0;
                canAdvanceCombo = false;
            }
            if (canAdvanceCombo)
            {
                comboState++;
                anim.SetTrigger("playerNextStrike");
                attackCollider.SetActive(true);
                AttackSlide();
                canAdvanceCombo = false;
            }
            else
            {
                comboState = 0;
            }
            yield return null;
        }
    }
    /*private IEnumerator Combo()
    {
        attackCollider.SetActive(true);
        anim.SetTrigger("playerStrikea");
        yield return new WaitUntil(() => comboState > 0 || !isAttacking);
        while (comboState > 0)
        {
            //Oyuncuyu düşmana doğru kaydıran methodu çağırır.
            AttackSlide();
            //float forceMultiplier = 1;
            /*foreach (GameObject enemy in enemyList)
            {
                GameObject enemyParent;
                enemyParent = enemy.transform.parent.gameObject;
                enemyParent.GetComponent<EnemyAI>().playerSeen = true;
                enemyParent.GetComponent<Rigidbody2D>().drag = 8f;
                if (enemyParent.GetComponent<EnemyAI>().currentState != EnemyAI.EnemyState.Attacking)
                    enemyParent.GetComponent<EnemyAI>().currentState = EnemyAI.EnemyState.Stunned;
                if (transform.position.x > enemyParent.transform.position.x)
                {
                    enemyParent.GetComponent<Rigidbody2D>().velocity = new Vector2(-knockbackForce * forceMultiplier, knockupForce);
                    enemyParent.transform.localScale = enemyParent.GetComponent<EnemyAI>().faceRight;
                }
                else if (transform.position.x < enemyParent.transform.position.x)
                {
                    enemyParent.GetComponent<Rigidbody2D>().velocity = new Vector2(knockbackForce * forceMultiplier, knockupForce);
                    enemyParent.transform.localScale = enemyParent.GetComponent<EnemyAI>().faceLeft;
                }
            }

            yield return new WaitForSeconds(0.4f);
            if (comboState >= 3)
            {
                //forceMultiplier = 3;
                //canCombo = false;
                comboState = 0;
            }
            //GameObject comboedEnemy;
            //comboedEnemy = enemyList[0];
            //enemyList.Clear();
            //enemyList.Add(comboedEnemy);
            //Yarım saniye içinde tekrar Attack tuşuna basılmazsa (Basılıp basılmadığı update'den her frame kontrol ediliyor), combo sona erer ve oyuncu tekrar özgür hareket edebilir. Basılırsa kombo ilerler.
            if (canAdvanceCombo)
            {
                //Rigidbody2D rb;
                //rb = enemyList[0].GetComponentInParent<Rigidbody2D>();
                anim.SetTrigger("playerNextStrike");
                attackCollider.SetActive(true);
                AttackSlide();
                comboState++;
                canAdvanceCombo = false;
                //rb.velocity = new Vector2(0f, rb.velocity.y);
                //rb.drag = 0f;
            }
            else
            {
                //Rigidbody2D rb;
                //rb = enemyList[0].GetComponentInParent<Rigidbody2D>();
                comboState = 0;
                //canCombo = false;
                //rb.velocity = new Vector2(0f, rb.velocity.y);
                //rb.drag = 0f;
                //enemyList.Clear();
                //enemy.GetComponentInParent<EnemyAI>().currentState = EnemyAI.EnemyState.Chasing;
            }
            yield return null;
        }
    }*/
}
