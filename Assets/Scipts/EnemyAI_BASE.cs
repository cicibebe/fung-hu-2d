﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody2D))]
public class EnemyAI_BASE : MonoBehaviour, IEnemyStateMachine
{
    [SerializeField]
    private EnemyProperties properties;

    [SerializeField]
    private EnemyState originState;

    public EnemyState currentState;
    protected int hitState;

    public float defaultSpeed = 1f;
    protected float currentSpeed;

    protected Vector3 originPosition;

    //RequiredComponents
    protected EnemyVision vision;
    protected Rigidbody2D enemyrb2d;
    protected Animator anim;
    protected GameObject player;


    protected Vector2 localScaleTo_faceLeft;
    protected Vector2 localScaleTo_faceRight;
    protected PlayerProperties playerProperties;

    public EnemyProperties Properties
    {
        get
        {
            return properties;
        }
    }

    public virtual IEnumerator EnemyStateMachine() { throw new System.NotImplementedException(); }




    public void Init()
    {
        vision = GetComponentInChildren<EnemyVision>();
        enemyrb2d = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        originPosition = transform.position;
        localScaleTo_faceRight = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        localScaleTo_faceLeft = new Vector2(transform.localScale.x, transform.localScale.y);
        playerProperties = GameManager.Instance.PlayerProperties;
        player = GameManager.Instance.Player;

        currentSpeed = defaultSpeed;
        currentState = originState;
        StartCoroutine("EnemyStateMachine");
    }

    //Patrol sekansı sırasında dönüş zamanı geldiğinde sprite'ını ve ivmesini mirror etmek için.
    protected virtual void Flip()
    {
        Debug.Log("Flip");
        enemyrb2d.velocity = new Vector2(currentSpeed, enemyrb2d.velocity.y);
        transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        currentSpeed = currentSpeed * -1;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "PlayerAttackCollider")
        {
            GotHit(playerProperties.damage);
        }
    }

    public void GotHit(float damage)
    {
        Debug.Log("GotHit");
        properties.hitPoints -= damage;

        if (properties.hitPoints <= 0)
        {
            Die();
        }
        else if (currentState != EnemyState.Attacking)
        {
            SetEnemyStateWhenGettingHit();
        }
    }

    protected void SetEnemyStateWhenGettingHit()
    {
        StopCoroutine("EnemyStateMachine");
        if (hitState < playerProperties.maxComboState)
        {
            currentState = EnemyState.Hitstunned;
        }
        else if (hitState >= playerProperties.maxComboState)
        {
            currentState = EnemyState.Stunned;
        }
        StartCoroutine("EnemyStateMachine");
    }

    public void Die()
    {
        StopCoroutine("EnemyStateMachine");
        gameObject.SetActive(false);
    }


    public virtual IEnumerator Attack()
    {
        throw new System.NotImplementedException();
    }

    public virtual IEnumerator Chase()
    {
        throw new System.NotImplementedException();
    }

    public virtual IEnumerator Patrol()
    {
        throw new System.NotImplementedException();
    }

    public virtual IEnumerator Hitstun()
    {
        throw new System.NotImplementedException();
    }

    public virtual IEnumerator ReturnToPatrol()
    {
        throw new System.NotImplementedException();
    }

    public virtual IEnumerator Search()
    {
        throw new System.NotImplementedException();
    }

    public virtual IEnumerator Stun()
    {
        throw new System.NotImplementedException();
    }

    public virtual IEnumerator Wait()
    {
        throw new System.NotImplementedException();
    }

    public virtual IEnumerator Recover()
    {
        throw new System.NotImplementedException();
    }

    public virtual IEnumerator KnockBack()
    {
        throw new System.NotImplementedException();
    }

}

public enum EnemyState
{
    Patrolling,
    ReturningToPatrol,
    Searching,
    Waiting,
    Chasing,
    Attacking,
    Recovering,
    Hitstunned,
    Stunned,
    KnockedBack
}

[System.Serializable]
public class EnemyProperties
{
    public float hitPoints = 3;
    public float speed = 1;
    public float damage = 1;
}