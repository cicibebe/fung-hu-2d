﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

    private static PlayerController instance;
    public static PlayerController Instance
    {
        get
        {
            return instance;
        }
    }

    public float speed = 3f;
    public float jumpForce = 6f;
    public Transform groundCheck;
    public Animator anim;

    private Rigidbody2D playerrb2d;
    public bool isGrounded = true;
    private bool doubleJump = false;
    public bool facingRight = true;

    private PlayerAttack playerAttack;

    private int comboState;
    [SerializeField]
    private float hitPoints;


    //public PlayerState currentState;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            DestroyImmediate(gameObject);

        playerAttack = GetComponent<PlayerAttack>();
        playerrb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        hitPoints = GameManager.Instance.PlayerProperties.totalHitPoints;
    }

    void Start ()
    {
        

        //currentState = PlayerState.Idle;

        //StartCoroutine(PlayerStateMachine());
	}

    /*IEnumerator PlayerStateMachine()
    {
        while (this) {
            switch (currentState)
            {
                case PlayerState.Idle:
                    yield return null;
                    break;
                case PlayerState.Running:
                    yield return null;
                    break;
                case PlayerState.Jumping:
                    yield return null;
                    break;
                case PlayerState.Attacking:
                    yield return null;
                    break;
            }
        }
    }*/


    // Update is called once per frame
    void Update () {

        comboState = playerAttack.comboState;
        if (comboState == 0)
        {
            isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

            if (CrossPlatformInputManager.GetButtonDown("Jump"))
            {
                anim.SetBool("isLanding", false);
                if (!doubleJump && isGrounded)
                {
                    Invoke("Landing", 0.5f);
                    anim.Play("Player_Jump_anim");
                    doubleJump = true;
                    playerrb2d.velocity = new Vector2(playerrb2d.velocity.x, jumpForce);
                }
                else if (doubleJump)
                {
                    CancelInvoke();
                    Invoke("Landing", 0.5f);
                    anim.Play("Player_Jump_anim");
                    doubleJump = false;
                    playerrb2d.velocity = new Vector2(playerrb2d.velocity.x, jumpForce);
                }
            }

            float h = CrossPlatformInputManager.GetAxisRaw("Horizontal");

            playerrb2d.velocity = new Vector2(h * speed, playerrb2d.velocity.y);

            if (h != 0)
                anim.SetBool ("isRunning", true);
            else
                anim.SetBool ("isRunning", false);

            if (h > 0 && !facingRight)
                Flip();
            else if (h < 0 && facingRight)
                Flip();
        }
        else if (comboState > 0)
            playerrb2d.velocity = new Vector2(0, 0);
    }

    void Landing()
    {
        anim.SetBool("isLanding", true);
    }

    public void GotHit(float damage)
    {
        hitPoints -= damage;
        EventManager.PlayerGotHit(hitPoints);
        if (hitPoints <= 0)
            Die();

        //TODO: GotHit Anim 
    }

    void Die()
    {
        //TODO: Die Anim 
        EventManager.PlayerDied();
    }

    void Flip(){
        facingRight = !facingRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

    /*public enum PlayerState
    {
        Idle,
        Running,
        Jumping,
        Attacking,
    }*/
}